﻿using AutoMapper;
using DataBase.Model;
using Services.DTO;

namespace Services.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<BoardDto, Board>(MemberList.Source).ReverseMap();
            CreateMap<CardDto, Card>(MemberList.Source).ReverseMap();
            CreateMap<ColumnDto, Column>(MemberList.Source).ReverseMap();
            CreateMap<CommentDto, Comment>(MemberList.Source).ReverseMap();
            CreateMap<LabelDto, Label>(MemberList.Source).ReverseMap();
            CreateMap<PermissionDto, Permission>(MemberList.Source).ReverseMap();
            CreateMap<UserDto, User>(MemberList.Source).ReverseMap();
        }
    }
}
