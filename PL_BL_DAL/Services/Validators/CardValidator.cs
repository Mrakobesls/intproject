﻿using FluentValidation;
using Services.DTO;

namespace Services.Validators
{
    public class CardValidator : AbstractValidator<CardDto>
    {
        public CardValidator()
        {
            RuleFor(b => b.Id).NotNull().GreaterThan(0);
            RuleFor(b => b.ColumnId).NotNull().GreaterThan(0);
            RuleFor(b => b.Title).NotNull().MaximumLength(20);
            RuleFor(b => b.Description).NotNull().MaximumLength(100);
        }
    }
}
