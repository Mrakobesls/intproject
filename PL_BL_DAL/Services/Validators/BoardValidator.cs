﻿using FluentValidation;
using Services.DTO;

namespace Services.Validators
{
    public class BoardValidator : AbstractValidator<BoardDto>
    {
        public BoardValidator()
        {
            RuleFor(b => b.Id).GreaterThan(0);
            RuleFor(b => b.Title).NotNull().MaximumLength(20);
            RuleFor(b => b.Description).NotNull().MaximumLength(100);
        }
    }
}
