﻿using FluentValidation;
using Services.DTO;

namespace Services.Validators
{
    public class LabelValidator : AbstractValidator<LabelDto>
    {
        public LabelValidator()
        {
            RuleFor(b => b.Id).NotNull().GreaterThan(0);
            RuleFor(b => b.Name).NotNull().MaximumLength(20);
        }
    }
}
