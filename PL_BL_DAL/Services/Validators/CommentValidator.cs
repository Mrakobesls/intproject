﻿using FluentValidation;
using Services.DTO;

namespace Services.Validators
{
    public class CommentValidator : AbstractValidator<CommentDto>
    {
        public CommentValidator()
        {
            RuleFor(b => b.Id).NotNull().GreaterThan(0);
            RuleFor(b => b.CardId).NotNull().GreaterThan(0);
            RuleFor(b => b.UserId).NotNull().GreaterThan(0);
            RuleFor(b => b.Text).NotNull().MaximumLength(200);
        }
    }
}
