﻿using FluentValidation;
using FluentValidation.Validators;
using Services.DTO;

namespace Services.Validators
{
    public class UserValidator : AbstractValidator<UserDto>
    {
        public UserValidator()
        {
            RuleFor(b => b.Id).NotNull().GreaterThanOrEqualTo(0);
            RuleFor(b => b.Email).NotNull().MaximumLength(30);
            RuleFor(b => b.Login).NotNull().MaximumLength(20);
            RuleFor(b => b.Password).NotNull().MaximumLength(20);
            RuleFor(b => b.FirstName).NotNull().MaximumLength(25);
            RuleFor(b => b.LastName).NotNull().MaximumLength(25);
        }
    }
}
