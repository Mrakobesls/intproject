﻿using System;
using System.Linq;
using AutoMapper;
using DataBase.Model;
using FluentValidation;
using Repository.Interfaces;
using Services.DTO;
using Services.Services.Intefaces;

namespace Services.Services
{
    public class CardService : IService<CardDto>
    {
        private readonly IGenericRepository<Card> _repository;
        private readonly IMapper _mapper;
        private readonly IValidator<CardDto> _validator;
        public CardService(IGenericRepository<Card> repository, IMapper mapper, IValidator<CardDto> validator)
        {
            _repository = repository;
            _mapper = mapper;
            _validator = validator;
        }
        public void Create(CardDto instance)
        {
            Validate(instance);
            _repository.Create(_mapper.Map<Card>(instance));
        }
        public CardDto Read(int id)
        {
            var temp = _repository.Read(id);
            return _mapper.Map<CardDto>(temp);
        }
        public IQueryable<CardDto> ReadAll()
        {
            return _repository
                    .ReadAll()
                    .Select(b => _mapper.Map<CardDto>(b));
        }
        public CardDto Update(CardDto instance)
        {
            Validate(instance);
            return _mapper.Map<CardDto>(_repository
                    .Update(_mapper.Map<Card>(instance)));
        }
        public virtual void Delete(CardDto instance)
        {
            Validate(instance);
            _repository.Delete(_mapper.Map<Card>(instance));
        }

        public void Validate(CardDto instance)
        {
            var validity = _validator.Validate(instance);
            if (!validity.IsValid)
                throw new ArgumentOutOfRangeException(validity.ToString());
        }
    }
}
