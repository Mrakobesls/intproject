﻿using System;
using System.Linq;
using AutoMapper;
using DataBase.Model;
using FluentValidation;
using Repository.Interfaces;
using Services.DTO;
using Services.Services.Intefaces;

namespace Services.Services
{
    public class BoardService : IService<BoardDto>
    {
        private readonly IGenericRepository<Board> _repository;
        private readonly IMapper _mapper;
        private readonly IValidator<BoardDto> _validator;
        public BoardService(IGenericRepository<Board> repository, IMapper mapper, IValidator<BoardDto> validator)
        {
            _repository = repository;
            _mapper = mapper;
            _validator = validator;
        }
        public void Create(BoardDto instance)
        {
            Validate(instance);
            _repository.Create(_mapper.Map<Board>(instance));
        }
        public BoardDto Read(int id)
        {
            var temp = _repository.Read(id);
            return _mapper.Map<BoardDto>(temp);
        }
        public IQueryable<BoardDto> ReadAll()
        {
            return _repository
                    .ReadAll()
                    .Select(b => _mapper.Map<BoardDto>(b));
        }
        public BoardDto Update(BoardDto instance)
        {
            Validate(instance);
            return _mapper.Map<BoardDto>(_repository
                    .Update(_mapper.Map<Board>(instance)));
        }
        public virtual void Delete(BoardDto instance)
        {
            Validate(instance);
            _repository.Delete(_mapper.Map<Board>(instance));
        }

        public void Validate(BoardDto instance)
        {
            var validity = _validator.Validate(instance);
            if (!validity.IsValid)
                throw new ArgumentOutOfRangeException(validity.ToString());
        }
    }
}
