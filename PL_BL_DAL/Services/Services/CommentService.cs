﻿using System;
using System.Linq;
using AutoMapper;
using DataBase.Model;
using FluentValidation;
using Repository.Interfaces;
using Services.DTO;
using Services.Services.Intefaces;

namespace Services.Services
{
    public class CommentService : IService<CommentDto>
    {
        private readonly IGenericRepository<Comment> _repository;
        private readonly IMapper _mapper;
        private readonly IValidator<CommentDto> _validator;
        public CommentService(IGenericRepository<Comment> repository, IMapper mapper, IValidator<CommentDto> validator)
        {
            _repository = repository;
            _mapper = mapper;
            _validator = validator;
        }
        public void Create(CommentDto instance)
        {
            Validate(instance);
            _repository.Create(_mapper.Map<Comment>(instance));
        }
        public CommentDto Read(int id)
        {
            var temp = _repository.Read(id);
            return _mapper.Map<CommentDto>(temp);
        }
        public IQueryable<CommentDto> ReadAll()
        {
            return _repository
                .ReadAll()
                .Select(b => _mapper.Map<CommentDto>(b));
        }
        public CommentDto Update(CommentDto instance)
        {
            Validate(instance);
            return _mapper.Map<CommentDto>(_repository
                .Update(_mapper.Map<Comment>(instance)));
        }
        public virtual void Delete(CommentDto instance)
        {
            Validate(instance);
            _repository.Delete(_mapper.Map<Comment>(instance));
        }

        public void Validate(CommentDto instance)
        {
            var validity = _validator.Validate(instance);
            if (!validity.IsValid)
                throw new ArgumentOutOfRangeException(validity.ToString());
        }
    }
}
