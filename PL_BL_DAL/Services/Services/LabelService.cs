﻿using System;
using System.Linq;
using AutoMapper;
using DataBase.Model;
using FluentValidation;
using Repository.Interfaces;
using Services.DTO;
using Services.Services.Intefaces;

namespace Services.Services
{
    public class LabelService : IService<LabelDto>
    {
        private readonly IGenericRepository<Label> _repository;
        private readonly IMapper _mapper;
        private readonly IValidator<LabelDto> _validator;
        public LabelService(IGenericRepository<Label> repository, IMapper mapper, IValidator<LabelDto> validator)
        {
            _repository = repository;
            _mapper = mapper;
            _validator = validator;
        }
        public void Create(LabelDto instance)
        {
            Validate(instance);
            _repository.Create(_mapper.Map<Label>(instance));
        }
        public LabelDto Read(int id)
        {
            var temp = _repository.Read(id);
            return _mapper.Map<LabelDto>(temp);
        }
        public IQueryable<LabelDto> ReadAll()
        {
            return _repository
                .ReadAll()
                .Select(b => _mapper.Map<LabelDto>(b));
        }
        public LabelDto Update(LabelDto instance)
        {
            Validate(instance);
            return _mapper.Map<LabelDto>(_repository
                .Update(_mapper.Map<Label>(instance)));
        }
        public virtual void Delete(LabelDto instance)
        {
            Validate(instance);
            _repository.Delete(_mapper.Map<Label>(instance));
        }

        public void Validate(LabelDto instance)
        {
            var validity = _validator.Validate(instance);
            if (!validity.IsValid)
                throw new ArgumentOutOfRangeException(validity.ToString());
        }
    }
}
