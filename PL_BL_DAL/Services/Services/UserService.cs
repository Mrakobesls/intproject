﻿using System;
using System.Linq;
using AutoMapper;
using DataBase.Model;
using FluentValidation;
using Repository.Interfaces;
using Services.DTO;
using Services.Services.Intefaces;

namespace Services.Services
{
    public class UserService : IService<UserDto>
    {
        private readonly IGenericRepository<User> _repository;
        private readonly IMapper _mapper;
        private readonly IValidator<UserDto> _validator;
        public UserService(IGenericRepository<User> repository, IMapper mapper, IValidator<UserDto> validator)
        {
            _repository = repository;
            _mapper = mapper;
            _validator = validator;
        }
        public void Create(UserDto instance)
        {
            Validate(instance);
            _repository.Create(_mapper.Map<User>(instance));
        }
        public UserDto Read(int id)
        {
            var temp = _repository.Read(id);
            return _mapper.Map<UserDto>(temp);
        }
        public IQueryable<UserDto> ReadAll()
        {
            return _repository
                .ReadAll()
                .Select(b => _mapper.Map<UserDto>(b));
        }
        public UserDto Update(UserDto instance)
        {
            Validate(instance);
            return _mapper.Map<UserDto>(_repository
                .Update(_mapper.Map<User>(instance)));
        }
        public virtual void Delete(UserDto instance)
        {
            Validate(instance);
            _repository.Delete(_mapper.Map<User>(instance));
        }

        public void Validate(UserDto instance)
        {
            var validity = _validator.Validate(instance);
            if (!validity.IsValid)
                throw new ArgumentOutOfRangeException(validity.ToString());
        }
    }
}
