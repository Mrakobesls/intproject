﻿namespace Services.Services.Intefaces
{
    public interface IValidate<T>
        where T : class
    {
        void Validate(T instance);
    }
}
