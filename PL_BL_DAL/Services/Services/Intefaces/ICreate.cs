﻿namespace Services.Services.Intefaces
{
    public interface ICreate<T>
        where T : class
    {
        void Create(T instance);
    }
}
