﻿using System.Linq;

namespace Services.Services.Intefaces
{
    public interface IRead<T>
        where T : class
    {
        T Read(int id);
        IQueryable<T> ReadAll();
    }
}
