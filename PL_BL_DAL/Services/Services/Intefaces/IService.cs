﻿namespace Services.Services.Intefaces
{
    public interface IService<T> : ICreate<T>, IRead<T>, IUpdate<T>, IDelete<T>, IValidate<T>
        where T : class
    {
    }
}
