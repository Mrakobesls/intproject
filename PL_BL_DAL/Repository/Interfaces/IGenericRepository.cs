﻿using System.Linq;

namespace Repository.Interfaces
{
    public interface IGenericRepository<T>
        where T : class
    {
        void Create(T instance);
        T Read(int id);
        IQueryable<T> ReadAll();
        T Update(T instance);
        void Delete(T instance);
    }
}
