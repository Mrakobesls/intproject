﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DataBase.Model
{
    [Index(nameof(FirstName), nameof(LastName))]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Email { get; set; }
        
        [Required]
        [MaxLength(20)]
        public string Login { get; set; }
        
        [Required]
        [MaxLength(20)]
        public string Password { get; set; }
        
        [Required]
        [MaxLength(25)]
        public string FirstName { get; set; }
        
        [Required]
        [MaxLength(25)]
        public string LastName { get; set; }
        
        public virtual ICollection<BoardAccess> BoardAccesses { get; set; } = new HashSet<BoardAccess>();
        public virtual ICollection<Card> Cards { get; set; } = new HashSet<Card>();
        public virtual ICollection<Comment> Comments { get; set; } = new HashSet<Comment>();
    }
}
