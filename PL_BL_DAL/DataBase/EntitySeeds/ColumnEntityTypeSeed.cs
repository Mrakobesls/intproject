﻿using DataBase.EntitySeeds.Inteface;
using DataBase.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.EntitySeeds
{
    internal class ColumnEntityTypeSeed : ISeed<Column>
    {
        public void Seed(EntityTypeBuilder<Column> builder)
        {
            builder.HasData(
                new Column() { Id = 1, BoardId = 1, Title = "Test column 1" },
                new Column() { Id = 2, BoardId = 1, Title = "Test column 2" }
            );
        }
    }
}
