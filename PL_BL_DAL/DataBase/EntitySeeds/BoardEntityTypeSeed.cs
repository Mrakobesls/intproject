﻿using DataBase.EntitySeeds.Inteface;
using DataBase.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.EntitySeeds
{
    internal class BoardEntityTypeSeed : ISeed<Board>
    {
        public void Seed(EntityTypeBuilder<Board> builder)
        {
            builder.HasData(
                new Board() { Id = 1, Title = "Mystic Board", Description = "Test board" }
            );
        }
    }
}
