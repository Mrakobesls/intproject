﻿using DataBase.EntitySeeds.Inteface;
using DataBase.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.EntitySeeds
{
    internal class PermissionEntityTypeSeed : ISeed<Permission>
    {
        public void Seed(EntityTypeBuilder<Permission> builder)
        {
            builder.HasData(
                new Permission() { Id = 1, Name = "Creator" },
                new Permission() { Id = 2, Name = "Owner" },
                new Permission() { Id = 3, Name = "User" }
            );
        }
    }
}
