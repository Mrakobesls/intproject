﻿using DataBase.EntitySeeds.Inteface;
using DataBase.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.EntitySeeds
{
    internal class CardEntityTypeSeed : ISeed<Card>
    {
        public void Seed(EntityTypeBuilder<Card> builder)
        {
            builder.HasData(
                new Card() { Id = 1, ColumnId = 1, Title = "Hello everyone", Description = "Hello everyone" },
                new Card() { Id = 2, ColumnId = 2, Title = "Task1", Description = "Task1" },
                new Card() { Id = 3, ColumnId = 2, Title = "Task2", Description = "Task2" }
            );
        }
    }
}
