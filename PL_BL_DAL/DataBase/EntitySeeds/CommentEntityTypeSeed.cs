﻿using System;
using DataBase.EntitySeeds.Inteface;
using DataBase.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.EntitySeeds
{
    internal class CommentEntityTypeSeed : ISeed<Comment>
    {
        public void Seed(EntityTypeBuilder<Comment> builder)
        {
            builder.HasData(
                new Comment() { Id = 1, CardId = 1, UserId = 2, Text = "Hello", DateTime = DateTime.Now },
                new Comment() { Id = 2, CardId = 1, UserId = 3, Text = "Hi", DateTime = DateTime.Now },
                new Comment() { Id = 3, CardId = 2, UserId = 3, Text = "Do smth!", DateTime = DateTime.Now }
            );
        }
    }
}
