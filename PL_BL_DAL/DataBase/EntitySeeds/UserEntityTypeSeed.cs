﻿using DataBase.EntitySeeds.Inteface;
using DataBase.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.EntitySeeds
{
    internal class UserEntityTypeSeed : ISeed<User>
    {
        public void Seed(EntityTypeBuilder<User> builder)
        {
            builder.HasData(
                new User()
                {
                    Id = 1,
                    Email = "admin@admin.by",
                    Login = "Admin",
                    Password = "admin",
                    FirstName = "admin",
                    LastName = "admin"
                },
                new User()
                {
                    Id = 2,
                    Email = "Admin2@admin.by",
                    Login = "Admin2",
                    Password = "Admin2",
                    FirstName = "Admin2",
                    LastName = "Admin2"
                },
                new User()
                {
                    Id = 3,
                    Email = "Admin3@admin.by",
                    Login = "Admin3",
                    Password = "Admin3",
                    FirstName = "Admin3",
                    LastName = "Admin3"
                });
        }
    }
}
